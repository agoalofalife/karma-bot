require('dotenv').config();

const findOneOrCreate = require('mongoose-find-one-or-create');
const mongoose = require('mongoose');
const connection = mongoose.connect(process.env.MONGO, { useMongoClient: true });

let SchemaUser = new mongoose.Schema({
    id: Number,
    slackId:  String,
    name: String,
    real_name: String,
    karma: Number,
    createdAt:{ type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
});
SchemaUser.plugin(findOneOrCreate);
let ModelUser = connection.model('User', SchemaUser);

module.exports = ModelUser;
