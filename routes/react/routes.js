const bot = require('../../bootstrap');
const KarmaController = require('../../Controllers/ReactControllers/KarmaController');

bot.on('message.im', (route, routeMention) => {
    route(/Спасибо.+\<\@.+\>/gi, KarmaController.thanksToSomeone);
    route(/^.*спасибо(?!.+\<\@.+\>).*$/gi, KarmaController.thanksNoOne);
});

