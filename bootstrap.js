const SlackBot = require('super-slack-bot');
const bot = new SlackBot({
    token: process.env.SLACK_BOT_TOKEN,
    name: process.env.SLACK_BOT_NAME,
});

module.exports = bot;