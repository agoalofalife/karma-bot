const ModelUser = require('./../../database/collections/users');

/**
 *
 * @type {module.KarmaController}
 */
module.exports = class KarmaController {
    /**
     *
     * @param response
     * @param classMessage
     * @return {Promise<void>}
     */
    static async thanksToSomeone(response, classMessage){
        let slackId = response.text.match(/\<\@(.+)\>/i)[1];
        let user = await classMessage.base.getUserById(slackId);

        ModelUser.findOneOrCreate({slackId: slackId}, {
            slackId: user.id,
            name: user.name,
            real_name: user.real_name || '',
            karma: 0,
        }, async (err, user) => {
            if (response.user === user.slackId) {
                await classMessage.reply('Ага я тебя расскусил! Себя не похвалишь, не кто не похвалит!');
            } else {
                if (user.slackId === await classMessage.base.getBotId()) {
                    await classMessage.reply('Низкий поклон, но меня благодарить не надо.');
                } else {
                    let totalKarma = user.karma + 1;
                    ModelUser.update({slackId:slackId}, {$set:{karma:totalKarma}}, async function (err, _) {
                        await classMessage.reply('Отлично добавлено в карму +1');
                        await classMessage.reply(`Карма у <@${slackId}> ` + '`' + totalKarma + '`');
                    });
                }
            }
        });
    }

    /**
     *
     * @param response
     * @param classMessage
     * @return {Promise<void>}
     */
    static async thanksNoOne(response, classMessage) {
        classMessage.reply('У нас принято благодарить того кто помог! Укажите кому именно спасибо!');
    }
};